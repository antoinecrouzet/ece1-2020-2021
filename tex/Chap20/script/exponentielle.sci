// Auteur : Crouzet
// Date : 13/07/2015
// Résumé : fonction simulant une loi exponentielle de paramètre lambda=0.5

clf; clc; // On efface et réinitialise la fenêtre graphique

N=10000;

lambda=0.5
titre='Loi exponentielle de paramètre lambda='+string(lambda)+' - N='+string(N)
    
x=0:0.1:10
y=lambda*exp(-lambda*x) // Densité de la loi exponentielle
t=grand(N,1,"exp",1/lambda) // exp : loi exponentielle. Attention, le paramètre est l'espérance

histplot(x, t, style=5); // On représente par classe d'écart 0.1 pour représentation
plot2d(x,y,rect=[0,0,8,lambda+0.1],style=1) // On représente la densité

xtitle("", "Classes", "Effectifs par classe normalisés")
legend(titre) 
a=get("current_axes"); 
a.font_size=1; 
a.x_location="bottom";
