\chapter{Espaces vectoriels}

\objectifintro{
Ce chapitre est très important et tombe régulièrement au concours. Il est abstrait, mais pas difficile. Il sera enrichi dans un prochain chapitre, et approfondi l'année prochaine. \\Il doit être maitrisé dans son ensemble.
}

\begin{objectifs}
\begin{numerote}
  \item Connaître la définition d'espaces vectoriels et de sous-espaces vectoriels :
\begin{itemize}
  \item \lienobj{1}{Savoir démontrer qu'un ensemble est un sous-espace vectoriel d'un espace vectoriel}
  \item \lienobj{2}{Savoir montrer qu'un vecteur est une combinaison linéaire d'autres vecteurs}
\end{itemize}
  \item Maitriser la notion de base :
\begin{itemize}
  \item \lienobj{3}{Savoir montrer qu'une famille est libre}
  \item \lienobj{4}{Savoir montrer qu'une famille est génératrice}
  \item \lienobj{5}{Savoir montrer qu'une famille est une base d'un espace vectoriel}
  \item \lienobj{6}{Connaître les bases canoniques des espaces usuels}
  \item \lienobj{7}{Savoir déterminer la dimension d'un sous-espace vectoriel}
%* \hyperref[objectif-18-8]{Savoir déterminer les coordonnées d'un vecteur dans une base}
\end{itemize}
\end{numerote}
\end{objectifs}


%%% Début du cours %%%
\section{Espaces vectoriels}

\subsection{Généralités}

\begin{definition}
Soit $E$ un ensemble non vide.
\begin{itemize}
  \item On dit que la loi $+$ est une \textbf{loi de composition interne} sur $E$ si $\forall~(x,y)\in E^2, x+y \in E$.
  \item On dit que la loi $\cdot$ est une \textbf{loi de composition externe} sur $E$ si $\forall~x\in E,\forall~\lambda \in \R, \lambda\cdot x \in E$.
\end{itemize}
\end{definition}

\begin{exemple}
L'exemple le plus classique est l'ensemble des matrices $\MM_{n,p}(\R)$. La loi d'addition de matrices est une loi de composition interne, et la multiplication par un réel est une loi de composition externe.
\end{exemple}

\begin{definition}
Soit $E$ un ensemble non vide, muni d'une loi interne, noté $+$, et d'une loi externe, noté $\cdot$. On dit que $E$ est un \textbf{espace vectoriel} sur $\R$ si les lois vérifient les propriétés suivantes :
\begin{itemize}
  \item (commutativité de $+$) : $\forall~(x,y) \in E^2, x+y=y+x$.
  \item (associativité de $+$) : $\forall~(x,y,z)\in E^3, (x+y)+z=x+(y+z)$
  \item (neutre pour $+$) : il existe un élément, noté $0_E$, tel que $\forall~x \in E, x+0_E=0_E+x=x$.
  \item  (inverse pour $+$) : pour tout $x \in E$, il existe un élément $y \in E$, tel que $x+y=y+x=0_{E}$.  Cet élément est appelé \textit{opposé} de $x$, et est noté $-x$.
  \item (neutre pour $\cdot$) $\forall~x\in E, 1.x = x$
  \item (distributivité de $\cdot$) $\forall~\lambda\in \R, \forall~(x,y)\in E^2, \lambda.(x+y)=\lambda.x+\lambda.y$
  \item (distributivité de $\cdot$) $\forall~(\lambda, \mu)\in \R^2, \forall~x\in E, (\lambda+\mu).x=\lambda.x+\mu.x$
  \item $\forall~(\lambda, \mu)\in \R^2, \forall~x\in E, \lambda.(\mu.x)=(\lambda\times\mu).x$.
\end{itemize}
\end{definition}

\begin{remarque}
Si $E$ est un espace vectoriel, les éléments de $E$ sont alors appelés les \textbf{vecteurs}, et les réels sont appelés les \textbf{scalaires}. L'élément $0_{E}$ est appelé vecteur nul.
\end{remarque}

\begin{remarque}
Les quatre premières propriétés font de $(E,+)$ ce qu'on appelle un \textbf{groupe abélien} ou groupe commutatif.
\end{remarque}

\begin{remarque}
Le symbole $\cdot$ de la loi de composition externe est très souvent omis. On notera plus souvent $2x$ plutôt que $2\cdot x$.
\end{remarque}

\begin{proposition}
[Exemple fondamental] Les ensembles $\MM_{n,1}(\R)$ pour $n\geq 1$, munis de l'addition de matrices, et de la multiplication par un réel, sont des espaces vectoriels.
\end{proposition}

\preuve[20]{
En effet, si
$A=\left( \begin{array}{c} a_1 \\\vdots \\a_n \end{array}\right) \qeq B=\left( \begin{array}{c} b_1 \\\vdots \\b_n \end{array}\right) $
alors
\[A+B=\left( \begin{array}{c} a_1+b_1 \\\vdots \\a_n+b_n \end{array}\right)=\left( \begin{array}{c} b_1+a_1 \\\vdots \\b_n+a_n \end{array}\right)=B+A\]
\[(A+B)+C=\left( \begin{array}{c} (a_1+b_1)+c_1 \\\vdots \\(a_n+b_n)+c_n \end{array}\right)=
\left( \begin{array}{c} a_1+(b_1+c_1) \\\vdots \\a_n+(b_n+c_n) \end{array}\right)=A+(B+C)\]
\[A+0_{n,1}= \left( \begin{array}{c} a_1+0 \\\vdots \\a_n+0 \end{array}\right)=\left( \begin{array}{c} 0+a_1 \\\vdots \\0+a_n \end{array}\right)=\left( \begin{array}{c} a_1 \\\vdots \\a_n \end{array}\right)=A\]
\[A+(-A)=\left( \begin{array}{c} a_1+(-a_1) \\\vdots \\a_n+(-a_n) \end{array}\right)=\left( \begin{array}{c} 0 \\\vdots \\0 \end{array}\right)=0_{n,1}\]
\[1.A=\left( \begin{array}{c} 1.a_1 \\\vdots \\1.a_n \end{array}\right)=\left( \begin{array}{c} a_1 \\\vdots \\a_n \end{array}\right)=A\]
\[\lambda.(A+B)=\lambda.\left( \begin{array}{c} a_1+b_1 \\\vdots \\a_n+b_n \end{array}\right)
    =\left( \begin{array}{c} \lambda (a_1+b_1) \\\vdots \\\lambda (a_n+b_n) \end{array}\right)    =\left( \begin{array}{c} \lambda a_1+\lambda b_1 \\\vdots \\\lambda a_n+\lambda b_n \end{array}\right)=\lambda.\left( \begin{array}{c} a_1 \\\vdots \\a_n \end{array}\right) + \lambda.\left( \begin{array}{c} b_1 \\\vdots \\b_n \end{array}\right)=\lambda.A+\lambda.B\]
\[(\lambda+\mu).A=\left( \begin{array}{c} (\lambda+\mu)a_1 \\\vdots \\(\lambda+\mu)a_n \end{array}\right)=\left( \begin{array}{c} \lambda a_1+\mu a_1 \\\vdots \\\lambda a_n+\mu a_n \end{array}\right)=\lambda.A+\mu.A\]
\[\lambda.(\mu.A) = \lambda.\left( \begin{array}{c} \mu a_1 \\\vdots \\\mu a_n \end{array}\right)
=\left( \begin{array}{c} \lambda \mu a_1 \\\vdots \\\lambda \mu a_n \end{array}\right) =(\lambda\times\mu).A\]
En première année, nous n'utiliserons que ces espaces vectoriels, dans le cas où $n=1,2,3$ ou $4$.
}

\subsection{Règles de calculs}

On se place ici dans un espace vectoriel $E$.

\begin{proposition}
Pour tous $\lambda \in \R$ et $x\in E$, on a
\begin{itemize}
  \item $\lambda.0_E=0_E$ et $0.x=0_E$
  \item $(-\lambda).x=\lambda.(-x)=-(\lambda.x)$.
  \item $x+(-y)=x-y$.
\end{itemize}
\end{proposition}

\begin{theoreme}
Soit $\lambda \in \R$, et $x\in E$. Alors
\[\lambda.x=0_E \Leftrightarrow x=0_E\quad \textrm{ou}\quad \lambda=0\]
\end{theoreme}

\subsection{Combinaison linéaire}

Soit $E$ un espace vectoriel.

\begin{definition}
On appelle \textbf{famille de vecteurs} de $E$ une $n$-liste $(e_1,\cdots, e_n)$ d'éléments de $E$.
\end{definition}


\begin{exemple}
Si $A=\left(\begin{array}{c} 1\\2 \end{array}\right)$ et
$B=\left(\begin{array}{c} 4\\-5 \end{array}\right)$, alors $(A,B)$ désigne une famille de deux vecteurs de $\MM_{2,1}(\R)$.
\end{exemple}

\begin{definition}
Soient $(e_1, \cdots, e_p)$ une famille de $p$ vecteurs de $E$. Soit $x$ un vecteur de $E$. On dit que $x$ est une \textbf{combinaison linéaire} de la famille $(e_1,\cdots, e_p)$ s'il existe des réels $(\lambda_1, \cdots, \lambda_p)$ tels que
\[x=\sum_{i=1}^p \lambda_i e_i = \lambda_1 e_1+\cdots+ \lambda_p e_p\]
Les réels $\lambda_1,\cdots, \lambda_p$ sont alors les \textbf{coefficients} de la combinaison linéaire.
\end{definition}

\begin{remarque}
Il n'y a pas forcément unicité de la combinaison linéaire.
\end{remarque}

\begin{exemple}
Soient $A=\left( \begin{array}{c}1\\2\end{array}\right)$, $B=\left( \begin{array}{c}-3\\1\end{array}\right)$. Alors, $2.A-3.B=\left( \begin{array}{c}11\\1\end{array}\right)$ est une combinaison linéaire de $A$ et $B$.
\end{exemple}

\begin{methode}
\labelobj{2}
Pour montrer qu'un vecteur $x$ est combinaison linéaire d'une famille $(e_1,\cdots,e_p)$, on écrit $\ds{x=\sum_{i=1}^p \lambda_i e_i}$ et on résout un système pour déterminer (ou non) les réels $\lambda_1,\cdots,\lambda_p$.
\end{methode}

\begin{exo}
Notons $A=\left( \begin{array}{c}1\\2\end{array}\right)$, $B=\left( \begin{array}{c}-3\\1\end{array}\right)$ et $X=\left( \begin{array}{c}-5\\4\end{array}\right)$. Montrer que $X$ est combinaison linéaire de $A$ et $B$.
\end{exo}

\solution[7]{
On écrit $X=\lambda A + \mu B$. On a alors
\[\left( \begin{array}{c}-5\\4\end{array}\right)=\left( \begin{array}{c}\lambda -3\mu\\2\lambda+\mu\end{array}\right)\]
On résout alors le système :
\[\left\{ \begin{array}{ccccc} \lambda&-&3\mu&=&-5 \\ 2\lambda&+&\mu&=&4 \end{array}\right. \Leftrightarrow
\left\{ \begin{array}{ccccc} \lambda&-&3\mu&=&-5 \\ &&7\mu&=&14 \end{array}\right.
  \Leftrightarrow
  \left\{ \begin{array}{ccc} \lambda&=&1 \\ \mu&=&2 \end{array}\right.
\]
Ainsi, $X=A+2B$.
}

\section{Sous-espace vectoriel}

\subsection{Définition}

\begin{definition}
Soit $E$ un espace vectoriel. Soit $F$ un sous ensemble de $E$ \underline{non vide}. On dit que $F$ est un \textbf{sous-espace vectoriel} de $E$ si les restrictions des lois $+$ et $\cdot$ à $F$ font de $F$ un espace vectoriel.
\end{definition}

\begin{exemple}
Si $E$ est un espace vectoriel, alors $\{0_E\}$ et $E$  sont des sous-espaces vectoriels de $E$.
\end{exemple}

\begin{propriete}
Soit $F$ un sous-espace vectoriel d'un espace vectoriel $E$. Alors $0_E \in F$ et $F$ est un espace vectoriel.
\end{propriete}

\begin{demonstration}
Par définition, $F$ est un espace vectoriel. Par propriété, si $x\in F$ (puisque $F$ est non vide), alors $0.x \in F$ c'est-à-dire $0_E \in F$.
\end{demonstration}


\begin{proposition}
Soit $F$ un sous ensemble d'un espace vectoriel $E$. Alors, $F$ est un sous-espace vectoriel de $E$ si et seulement si
\begin{itemize}
  \item $F \neq \vide$
  \item $\forall~(x, y) \in F^2, x+y\in F$ ($F$ est stable par addition)
  \item $\forall~x\in F, \lambda \in \R, \lambda.x \in F$ ($F$ est stable par multiplication par un scalaire)
\end{itemize}
\end{proposition}

\begin{remarque}
\begin{itemize}
  \item La première propriété se vérifie en général en montrant que le neutre $0_E$ est dans $F$.
  \item Les deux dernières propriétés peuvent être regroupées en une seule :
    \[\forall~(x, y)\in F^2, \forall~\lambda \in \R, \lambda.x+y\in F\]
\end{itemize}
\end{remarque}

\begin{methode}
\labelobj{1}
On utilise la proposition précédente pour démontrer qu'un ensemble est un sous-espace vectoriel.
\end{methode}

\begin{exo}
Montrer que $F=\left\{ \left(\begin{array}{c} t \\ 0 \end{array}\right),~t\in \R \right\}$ est un sous-espace vectoriel de $\MM_{2,1}(\R)$.
\end{exo}

\solution[7]{
En effet,
\begin{itemize}
  \item $0_{2,1} \in F$ : il suffit de prendre $t=0$.
  \item Si $A=\left(\begin{array}{c} a \\ 0 \end{array}\right), B=\left(\begin{array}{c} b \\ 0 \end{array}\right)$ et $\lambda \in \R$, alors
    \[\lambda.A+B= \left(\begin{array}{c} \lambda a+b \\ 0 \end{array}\right) \in F\]
    en prenant $t=\lambda a+b$.
\end{itemize}
}

\begin{theoreme}
Soit $(\mathcal{S})$ un système linéaire homogène de $n$ équations à $n$ inconnues. L'ensemble des solutions de $(\mathcal{S})$ forme un sous-espace vectoriel de $\MM_{n,1}(\R)$
\end{theoreme}

\preuve[15]{
Soit $M\in \MM_{n}(\R)$ la matrice associée au système $\mathcal{S}$. Alors l'ensemble des solutions $F$ du système $(\mathcal{S})$  s'écrit également
\[F=\left \{ X \in \MM_{n,1}(\R),~MX=0_{n,1} \right \}\]
avec $0_{n,1} = \left( \begin{array}{c} 0\\\vdots \\0 \end{array}\right)$.
\begin{itemize}
  \item $F \subset \MM_{n,1}(\R)$.
  \item $0_{n,1} \in F$. En effet, $M.0_{n,1} = 0_{n,1}$ par définition de la matrice nulle.
  \item Soient $X$ et $Y$ dans $F$, et $\lambda \in \R$. Alors
    \[M.(\lambda X + Y) = \lambda M.X + M.Y = 0_{n,1} + 0_{n,1} = 0_{n,1}\]
    puisque $MX=MY=0_{n,1}$. Donc $\lambda X + Y \in F$.
\end{itemize}
$F$ est bien un sous-espace vectoriel de $\MM_{n,1}(\R)$.
}

 \afaire{Exercices \lienexo{1}, \lienexo{2}, \lienexo{3} et \lienexo{4}}

\subsection{Sous-espaces engendrés}

On se donne un espace vectoriel $E$.

\begin{definition}
Soient $(e_1, \cdots, e_p)$ une famille de vecteurs de $E$. On appelle \textbf{sous-espace vectoriel engendré par} $(e_1,\cdots,e_p)$, et on note $\Vect(e_1,\cdots, e_p)$, l'ensemble formé de toutes les combinaisons linéaires de $(e_1,\cdots, e_p)$. Ainsi,
\[x \in \Vect(e_1,\cdots,e_p) \Leftrightarrow\exists~(\lambda_1,\cdots, \lambda_p) \in \R^p,~~x=\lambda_1e_1+\cdots+\lambda_pe_p\]
\end{definition}

\begin{exemple}
Soit $A=\left( \begin{array}{c} 1 \\ 2 \end{array}\right)$. Alors \[\Vect(A) = \{ \lambda.A,~\lambda\in \R\}= \left\{ \left( \begin{array}{c} \lambda \\ 2\lambda \end{array}\right), \lambda \in \R\right\}\]
$\Vect(A)$ est appelée \textbf{droite vectorielle}.
\end{exemple}

\begin{theoreme}
Soit $(e_1, \cdots, e_p)$ une famille de vecteurs de $E$. Alors $\Vect(e_1,\cdots,e_p)$ est un sous-espace vectoriel de $E$.
\end{theoreme}

\begin{propriete}
Soit $(e_1, \cdots, e_p)$  une famille de vecteurs de $E$. Alors
\begin{itemize}
  \item $\forall~k\in \llbracket 1,p \rrbracket, e_k \in \Vect(e_1,\cdots,e_p)$
  \item $\Vect(e_1,\cdots,e_p)$ est le plus petit sous-espace vectoriel contenant la famille $(e_1,\cdots, e_p)$. Ainsi, si $F$ est un sous-espace vectoriel contenant $e_1,\cdots, e_p$, nécessairement, \\$\Vect(e_1,\cdots,e_p) \subset F$.
  \item Si $F=\Vect(e_1,\cdots, e_p, e_{p+1})$ et si $e_{p+1}$ est combinaison linéaire des vecteurs $e_1,\cdots, e_p$, alors $F=\Vect(e_1,\cdots,e_p)$.
  \item Si $(a_1,\cdots, a_p)$ sont des réels tous non nuls, et si $F=\Vect(a_1 e_1,\cdots, a_p e_p)$, alors \\$F=\Vect(e_1,\cdots, e_p)$.
\end{itemize}
\end{propriete}

\begin{exemple}
\begin{itemize}
  \item Soit $A=\left( \begin{array}{c} 2 \\ -4 \end{array}\right)$. Alors $\Vect(A)=\Vect\left(\left( \begin{array}{c} 1 \\ -2 \end{array} \right) \right)$.
  \item Soient $B=\left( \begin{array}{c} 0 \\ 1 \end{array}\right)$ et $C=\left( \begin{array}{c} 2 \\ -3 \end{array}\right)$. Alors $\Vect(A,B,C)=\Vect(A,B)$ puisque $C=A+B$.
  \item Soit $F$ un sous-espace vectoriel contenant les vecteurs $A$ et $B$ précédents. Alors, nécessairement, $\Vect(A,B) \subset F$.
\end{itemize}
\end{exemple}

 \afaire{Exercices \lienexo{5} et \lienexo{6}}

\section{Base d'un espace vectoriel}

\subsection{Définition}

\begin{definition}
Soit $E$ un espace vectoriel. Soit  $\mathcal{B}=(e_1,\cdots, e_p)$ une famille de vecteurs de $E$. \\
On dit que $\mathcal{B}$ est une \textbf{base} de $E$ si, pour tout vecteur $x$ de $E$, il existe une \textit{unique} $n$-liste $(\lambda_1,\cdots, \lambda_p)$ de réels tels que
\[x=\sum_{i=1}^p \lambda_i e_i\]
Les réels $(\lambda_1,\cdots, \lambda_p)$ sont appelés les \textbf{coordonnées} de $x$ dans la base $\mathcal{B}$
\end{definition}

\begin{exemple}
Si $F=\left\{ \left(\begin{array}{c} t \\ 0 \end{array}\right),~t\in \R \right\}$, alors tout élément de $F$ s'écrit de manière unique sous la forme $t.\left(\begin{array}{c} 1 \\ 0 \end{array}\right)$. Ainsi, la famille composée du vecteur $\left(\begin{array}{c} 1 \\ 0 \end{array}\right)$ est une base de $F$.
\end{exemple}

\begin{methode}
\labelobj{5}
Pour montrer qu'une famille $(e_1,\cdots,e_p)$ est une base d'un espace vectoriel $E$, on prend $x\in E$ et on résout $\lambda_1e_1+\cdots +\lambda_pe_p = x$. S'il existe une unique solution, alors $(e_1,\cdots, e_p)$ est bien une base de $E$.
\end{methode}

\begin{exemple}
Montrer que $\left(\left(\begin{array}{c}1\\2\end{array}\right), \left(\begin{array}{c} 1\\3 \end{array}\right)\right)$ est une base de $\MM_{2,1}(\R)$.
\end{exemple}

\solution[8]{
Soit $\left(\begin{array}{c} x\\y\end{array}\right)$ un élément de $\MM_{2,1}(\R)$. On cherche $a$ et $b$ tels que
\[a\left(\begin{array}{c}1\\2\end{array}\right)+b \left(\begin{array}{c} 1\\3 \end{array}\right)=\left(\begin{array}{c} x\\y\end{array}\right) \Leftrightarrow \left(\begin{array}{c}a+b\\2a+3b \end{array}\right)=\left(\begin{array}{c}x\\y\end{array}\right)\]
 On résout le système
 \[\left\{ \begin{array}{ccccc}a&+&b&=&x \\2a&+&3b&=&y \end{array}\right.
 \Leftrightarrow \left\{ \begin{array}{ccccc}a&+&b&=&x \\&&b&=&y-2x \end{array}\right.\]
 Le système est de Cramer, donc il possède une unique solution. \\
 \textbf{Bilan} : la famille $\left(\left(\begin{array}{c}1\\2\end{array}\right), \left(\begin{array}{c} 1\\3 \end{array}\right)\right)$ est bien une base de de $\MM_{2,1}(\R)$.

}

 \afaire{Exercices \lienexo{9} et \lienexo{10}}
 
 \subsection{Famille libre, famille génératrice}
 
Dans la définition d'une base, il y a deux éléments importants : l'existence d'une combinaison linéaire, et l'unicité de celle-ci. Cela nous amène à définir deux concepts :

\begin{definition}
Soit $E$ un espace vectoriel. Soit  $(e_1,\cdots, e_p)$ une famille de vecteurs de $E$.
\begin{itemize}
  \item On dit que la famille $(e_1,\cdots, e_p)$ est \textbf{libre} si
    \[\lambda_1 e_1+\cdots +\lambda_p e_p = 0_E \Rightarrow \lambda_1=\cdots=\lambda_p = 0\] Si elle n'est pas libre, on dit qu'elle est \textbf{liée}.
  \item On dit que la famille $(e_1,\cdots, e_p)$ est \textbf{génératrice} si $\Vect(e_1,\cdots,e_p)=E$.
\end{itemize}
\end{definition}

\begin{remarque}
Lorsqu'une famille est libre, cela implique que s'il y a une combinaison linéaire de ses éléments, celle-ci est forcément unique.

En effet, supposons que l'on puisse écrire \[ x = \sum_{k=1}^p \lambda_i e_i = \sum_{k=1}^p \mu_i e_i\]
Alors, en soustrayant les deux expressions de $x$, on obtient :
\[ \sum_{k=1}^p (\lambda_i-\mu_i) e_i = 0 \]
Par définition de la liberté, cela implique que pour tout $i\in \ll 1, p\rr$, $\lambda_i-\mu_i=0$, c'est-à-dire $\lambda_i=\mu_i$ : il y a bien unicité de la décomposition.
\end{remarque}

\begin{methode}
\labelobj{3}
Pour montrer qu'une famille $(e_1,\cdots,e_p)$ est libre, on écrit $\lambda_1e_1+\cdots \lambda_pe_p=0_E$ et on résout le système. S'il admet comme seule solution $(0,\cdots,0)$ alors elle est libre, sinon elle est liée.
\end{methode}

\begin{exemple}\label{\thechapter-exemple-libre-2}
Montrer que la famille $\left(\begin{array}{c}1\\1\\1\end{array}\right),\left(\begin{array}{c}2\\1\\0\end{array}\right)$ est libre.
\end{exemple}

\solution[6]{
On cherche $a$ et $b$ deux réels tels que
\[a\left(\begin{array}{c}1\\1\\1\end{array}\right)+b\left(\begin{array}{c}2\\1\\0\end{array}\right)=\left(\begin{array}{c}0\\0\\0\end{array}\right)\]
Ainsi,
\[\left(\begin{array}{c} a+2b\\a+b\\a \end{array}\right) = \left(\begin{array}{c}0\\0\\0\end{array}\right)\]
On obtient alors $a=0$ puis $b=0$. Ainsi, la famille est libre.
}

\begin{remarque}[Cas d'une famille de deux vecteurs]
Lorsqu'une famille est composée de deux vecteurs $(u,v)$, celle-ci est liée si et seulement si il existe $(\alpha, \beta)$ tels que $\alpha u +\beta v=0$, c'est-à-dire si un des vecteurs s'exprime comme un multiple de l'autre. Ainsi, si on voit rapidement que ce n'est pas le cas, on peut conclure : on signalera que les vecteurs ne sont pas colinéaires.

Par exemple, dans l'exercice \thechapter.\ref{\thechapter-exemple-libre-2}, les deux vecteurs ne sont clairement pas colinéaires (par exemple, en regardant le $0$ en 3\ieme position). Ainsi, la famille est libre.
\end{remarque}


 \afaire{Exercices \lienexo{7} et \lienexo{8}}

\begin{methode}
\labelobj{4}
Pour montrer qu'une famille $(e_1,\cdots,e_p)$ est génératrice, on écrit $\lambda_1e_1+\cdots \lambda_pe_p=x$ avec $x\in E$ et on résout le système. S'il admet au moins une solution alors elle est génératrice, sinon elle ne l'est pas et on exhibe alors un contre-exemple.
\end{methode}

\begin{remarque}
\begin{itemize}
  \item Une famille libre est une famille dans laquelle aucun vecteur ne peut être exprimé comme combinaison linéaire des autres vecteurs. Ainsi, tout vecteur est \og{}utile\fg{} dans la famille.
  \item Une famille génératrice est une famille qui permet de récupérer, par combinaison linéaire, tout vecteur de E; en revanche, plusieurs combinaisons linéaires peuvent mener au même vecteur : il n'y a pas forcément unicité de la décomposition.
\end{itemize}
\end{remarque}

\begin{theoreme}[Lien entre famille libre, génératrice et base]
Soit $E$ un espace vectoriel. Soit  $(e_1,\cdots, e_p)$ une famille de vecteurs de $E$. $(e_1,\cdots, e_p)$ est une base si et seulement si elle est libre et génératrice.
\end{theoreme}

\subsection{Base canonique de $\MM_{n,1}(\R)$}

\begin{remarque}
On s'intéresse à $\MM_{3,1}(\R)$. Toute matrice $A=\left( \begin{array}{c} a_1\\a_2\\a_3 \end{array} \right) \in \MM_{3,1}(\R)$ s'écrit de manière unique sous la forme
\[A=a_1\left(\begin{array}{c} 1 \\ 0 \\ 0 \end{array}\right)   +a_2\left(\begin{array}{c} 0 \\ 1 \\ 0 \end{array}\right)+a_3\left(\begin{array}{c} 0 \\ 0 \\ 1 \end{array}\right)\]
Ainsi, les trois matrices $e_1=\left(\begin{array}{c} 1 \\ 0 \\ 0 \end{array}\right), e_2=\left(\begin{array}{c} 0 \\ 1 \\ 0 \end{array}\right), e_3=\left(\begin{array}{c} 0 \\ 0 \\ 1 \end{array}\right)$ permettent de décrire toutes les matrices de $\MM_{3,1}(\R)$ :  $(e_1,e_2, e_3)$ représente une base de $\MM_{3,1}(\R)$.
\end{remarque}

\begin{definition}
\labelobj{6}
Soit $n\in \N^*$. On note $e_1=\left(\begin{array}{c} 1 \\ \vdots \\ 0 \end{array}\right)$, $e_2=\left(\begin{array}{c} 0 \\ 1\\\vdots \\ 0 \end{array}\right)$, ..., $e_n=\left(\begin{array}{c} 0 \\ \vdots\\ 0 \\ 1 \end{array}\right)$ des matrices de $\MM_{n,1}(\R)$.  Toute matrice de $\MM_{n,1}(\R)$ peut s'écrire de manière unique sous la forme \[a_1.e_1+\cdots+a_n.e_n\] où $(a_1,\cdots, a_n)\in \R^n$. La famille de vecteurs $(e_1, \cdots, e_n)$ forme une \textbf{base}, appelée \textbf{base canonique} de $\MM_{n,1}(\R)$.
\end{definition}

\begin{exemple}
[Cas $n=2,3,4$]
\begin{itemize}
  \item $\left(\left(\begin{array}{c} 1 \\ 0 \end{array}\right), \left(\begin{array}{c} 0\\ 1 \end{array}\right) \right)$ forme une base de $\MM_{2,1}(\R)$.
  \item $\left(\left(\begin{array}{c} 1 \\ 0\\0 \end{array}\right), \left(\begin{array}{c} 0\\ 1 \\0\end{array}\right), \left(\begin{array}{c} 0\\ 0 \\1\end{array}\right) \right)$ forme une base de $\MM_{3,1}(\R)$.
  \item $\left(\left(\begin{array}{c} 1 \\ 0\\0 \\0\end{array}\right), \left(\begin{array}{c} 0\\ 1 \\0\\0\end{array}\right), \left(\begin{array}{c} 0\\ 0 \\1\\0\end{array}\right), \left(\begin{array}{c} 0\\ 0 \\0\\1\end{array}\right) \right)$ forme une base de $\MM_{4,1}(\R)$.
\end{itemize}
\end{exemple}

\begin{remarque}
Il n'y a pas unicité de la base. Par exemple, dans $\MM_{2,1}(\R)$, les vecteurs $\left(\begin{array}{c} 2 \\ 0\end{array}\right)$ et $\left(\begin{array}{c} 0\\ -1 \end{array}\right)$ forment également une base.
\end{remarque}

\begin{definition}
Vous verrez l'année prochaine que toute base d'un espace vectoriel possède le même nombre d'éléments : ainsi, dans $\MM_{2,1}(\R)$, les bases possèdent $2$ vecteurs. On appelle ce nombre la \textbf{dimension} de l'espace vectoriel, et on le notera $\dim(E)$.
\end{definition}

\begin{exemple}
$\MM_{2,1}(\R)$ est de dimension $2$, et plus généralement $\MM_{n,1}(\R)$ est de dimension $n$.
\end{exemple}

\begin{methode}
\labelobj{7}
Pour déterminer la dimension d'un espace vectoriel, on détermine d'abord une base, et on conclut. Pour cela, on essaie de l'expliciter comme un espace vectoriel engendré par une certaine famille de vecteurs. On vérifie ensuite si celle-ci est libre.
\end{methode}

\begin{exemple}
Soit $F=\left\{ \left(\begin{array}{c}x\\y\end{array}\right),~x+y=0 \right \}$. Déterminer la dimension de $F$.
\end{exemple}

\solution[9]{
Remarquons qu'on peut écrire
\[F=\left\{ \left(\begin{array}{c}x\\y\end{array}\right),~y=-x \right \}\]
soit
\[F=\left\{ \left(\begin{array}{c}x\\-x\end{array}\right),~x\in \R \right \}\]
Ainsi,
\[F=\left\{ x\left(\begin{array}{c}1\\-1\end{array}\right),~x\in \R \right \}\]
et donc $\left(\begin{array}{c}1\\-1\end{array}\right)$ est une base de $F$ :
\[F=\Vect\left(\left(\begin{array}{c}1\\-1\end{array}\right)\right)\]
et $\dim(F) = 1$.
}

\afaire{Exercices \lienexo{11} et \lienexo{12}}
%
%%% Fin du cours %%%
