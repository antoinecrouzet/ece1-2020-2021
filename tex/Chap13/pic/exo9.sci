function y=f(x)
    y=(exp(x)-1)./(exp(x)+1)
endfunction

function  y=tangente(x)
    y=1/2.*x
endfunction

function y=as1(x)
    y=-1+x-x
endfunction

function y=as2(x)
    y=1+x-x
endfunction

x=linspace(-10,10,1000)
clf;
plot2d(x',[f(x); tangente(x); as1(x); as2(x)]',rect=[-10,-2,10,2])
a=gca();
a.x_location = "origin"; 
a.y_location = "origin"; 
a.children(1).children(1).foreground=1;
a.children(1).children(2).foreground=1;
a.children(1).children(1).line_style=2;
a.children(1).children(2).line_style=2;
poly2=a.children(1).children(4);
poly2.foreground=5;
poly2.thickness=3;
//a.x_ticks=tlist(["ticks", "locations", "labels"],[] ,[] ) ;
//a.y_ticks=tlist(["ticks", "locations", "labels"],[] ,[] ) ;
//xtitle("$\huge\mbox{Fonction }f$")
xs2pdf(0,"exo9")
