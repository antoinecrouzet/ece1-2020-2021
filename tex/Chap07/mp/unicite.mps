%!PS
%%BoundingBox: -1 -10 129 32 
%%HiResBoundingBox: -0.75 -9.9185 128.30905 31.74954 
%%Creator: MetaPost 2.00
%%CreationDate: 2020.11.04:1215
%%Pages: 1
%*Font: cmmi10 9.96265 9.96265 60:88
%*Font: cmsy7 6.97385 6.97385 30:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 1.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 21.25984 moveto
42.51968 21.25984 lineto stroke
newpath 85.03937 21.25984 moveto
127.55905 21.25984 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 21.25984 0 moveto
106.29921 0 lineto stroke
newpath 102.60326 -1.53094 moveto
106.29921 0 lineto
102.60326 1.53094 lineto
 closepath
gsave fill grestore stroke
newpath 24.9558 1.53094 moveto
21.25984 0 lineto
24.9558 -1.53094 lineto
 closepath
gsave fill grestore stroke
 1 0 0 setrgbcolor
19.1843 24.25984 moveto
(`) cmmi10 9.96265 fshow
 0 3 dtransform truncate idtransform setlinewidth pop
newpath 21.25984 21.25984 moveto 0 0 rlineto stroke
102.82611 24.25984 moveto
(`) cmmi10 9.96265 fshow
106.9772 27.87524 moveto
(0) cmsy7 6.97385 fshow
newpath 106.29921 21.25984 moveto 0 0 rlineto stroke
 0 0 0 setrgbcolor
61.18683 -9.9185 moveto
(d) cmmi10 9.96265 fshow
showpage
%%EOF
