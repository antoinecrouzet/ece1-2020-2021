%!PS
%%BoundingBox: -15 -27 298 121 
%%HiResBoundingBox: -14.42323 -26.48152 297.88774 120.02492 
%%Creator: MetaPost 2.00
%%CreationDate: 2020.11.04:1301
%%Pages: 1
%*Font: cmmi5 4.98132 4.98132 75:e
%*Font: cmr5 4.98132 4.98132 30:ffc
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -14.17323 0 moveto
297.63774 0 lineto stroke
newpath 293.941 -1.53128 moveto
297.63774 0 lineto
293.941 1.53128 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -14.17323 moveto
0 119.05501 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53105 115.35881 moveto
0 119.05501 lineto
-1.53105 115.35881 lineto
 closepath
gsave fill grestore stroke
 0.2 0.8 0 setrgbcolor 0 2 dtransform truncate idtransform setlinewidth pop
newpath 28.34645 -19.84242 moveto 0 0 rlineto stroke
24.29504 -24.98712 moveto
(u) cmmi5 4.98132 fshow
28.50964 -26.48152 moveto
(1) cmr5 4.98132 fshow
newpath 56.6929 18.42502 moveto 0 0 rlineto stroke
52.6415 13.28032 moveto
(u) cmmi5 4.98132 fshow
56.8561 11.78592 moveto
(2) cmr5 4.98132 fshow
newpath 85.03935 31.18127 moveto 0 0 rlineto stroke
80.98795 26.03658 moveto
(u) cmmi5 4.98132 fshow
85.20255 24.54218 moveto
(3) cmr5 4.98132 fshow
newpath 113.3858 37.55896 moveto 0 0 rlineto stroke
109.3344 32.41426 moveto
(u) cmmi5 4.98132 fshow
113.549 30.91986 moveto
(4) cmr5 4.98132 fshow
newpath 141.73225 41.386 moveto 0 0 rlineto stroke
137.68085 36.2413 moveto
(u) cmmi5 4.98132 fshow
141.89545 34.7469 moveto
(5) cmr5 4.98132 fshow
newpath 170.0787 43.93709 moveto 0 0 rlineto stroke
166.0273 38.79239 moveto
(u) cmmi5 4.98132 fshow
170.2419 37.29799 moveto
(6) cmr5 4.98132 fshow
newpath 198.42516 45.75934 moveto 0 0 rlineto stroke
194.37375 40.61464 moveto
(u) cmmi5 4.98132 fshow
198.58835 39.12024 moveto
(7) cmr5 4.98132 fshow
newpath 226.7716 47.12614 moveto 0 0 rlineto stroke
222.7202 41.98145 moveto
(u) cmmi5 4.98132 fshow
226.9348 40.48705 moveto
(8) cmr5 4.98132 fshow
newpath 255.11806 48.18887 moveto 0 0 rlineto stroke
251.06665 43.04417 moveto
(u) cmmi5 4.98132 fshow
255.28125 41.54977 moveto
(9) cmr5 4.98132 fshow
newpath 283.46451 49.03925 moveto 0 0 rlineto stroke
277.718 43.89455 moveto
(u) cmmi5 4.98132 fshow
281.9326 42.40015 moveto
(10) cmr5 4.98132 fshow
 0 0 1 setrgbcolor
newpath 28.34645 113.3858 moveto 0 0 rlineto stroke
23.8893 117.88022 moveto
(w) cmmi5 4.98132 fshow
28.91539 116.38582 moveto
(1) cmr5 4.98132 fshow
newpath 56.6929 85.03935 moveto 0 0 rlineto stroke
52.23575 89.53377 moveto
(w) cmmi5 4.98132 fshow
57.26184 88.03937 moveto
(2) cmr5 4.98132 fshow
newpath 85.03935 75.59068 moveto 0 0 rlineto stroke
80.5822 80.0851 moveto
(w) cmmi5 4.98132 fshow
85.60829 78.5907 moveto
(3) cmr5 4.98132 fshow
newpath 113.3858 70.86613 moveto 0 0 rlineto stroke
108.92865 75.36055 moveto
(w) cmmi5 4.98132 fshow
113.95474 73.86615 moveto
(4) cmr5 4.98132 fshow
newpath 141.73225 68.03131 moveto 0 0 rlineto stroke
137.2751 72.52573 moveto
(w) cmmi5 4.98132 fshow
142.3012 71.03133 moveto
(5) cmr5 4.98132 fshow
newpath 170.0787 66.14157 moveto 0 0 rlineto stroke
165.62155 70.63599 moveto
(w) cmmi5 4.98132 fshow
170.64764 69.14159 moveto
(6) cmr5 4.98132 fshow
newpath 198.42516 64.79207 moveto 0 0 rlineto stroke
193.968 69.28648 moveto
(w) cmmi5 4.98132 fshow
198.9941 67.79208 moveto
(7) cmr5 4.98132 fshow
newpath 226.7716 63.77951 moveto 0 0 rlineto stroke
222.31445 68.27393 moveto
(w) cmmi5 4.98132 fshow
227.34055 66.77953 moveto
(8) cmr5 4.98132 fshow
newpath 255.11806 62.99231 moveto 0 0 rlineto stroke
250.6609 67.48672 moveto
(w) cmmi5 4.98132 fshow
255.687 65.99232 moveto
(9) cmr5 4.98132 fshow
newpath 283.46451 62.3621 moveto 0 0 rlineto stroke
277.3123 66.85652 moveto
(w) cmmi5 4.98132 fshow
282.3384 65.36212 moveto
(10) cmr5 4.98132 fshow
 1 0 0 setrgbcolor
newpath 28.34645 45.35449 moveto 0 0 rlineto stroke
24.6035 40.2098 moveto
(v) cmmi5 4.98132 fshow
28.2012 38.7154 moveto
(1) cmr5 4.98132 fshow
newpath 56.6929 62.3621 moveto 0 0 rlineto stroke
52.94995 57.2174 moveto
(v) cmmi5 4.98132 fshow
56.54765 55.723 moveto
(2) cmr5 4.98132 fshow
newpath 85.03935 52.91344 moveto 0 0 rlineto stroke
81.2964 47.76874 moveto
(v) cmmi5 4.98132 fshow
84.8941 46.27434 moveto
(3) cmr5 4.98132 fshow
newpath 113.3858 59.52773 moveto 0 0 rlineto stroke
109.64285 54.38303 moveto
(v) cmmi5 4.98132 fshow
113.24055 52.88863 moveto
(4) cmr5 4.98132 fshow
newpath 141.73225 54.42514 moveto 0 0 rlineto stroke
137.9893 49.28044 moveto
(v) cmmi5 4.98132 fshow
141.587 47.78604 moveto
(5) cmr5 4.98132 fshow
newpath 170.0787 58.58264 moveto 0 0 rlineto stroke
166.33575 53.43794 moveto
(v) cmmi5 4.98132 fshow
169.93346 51.94354 moveto
(6) cmr5 4.98132 fshow
newpath 198.42516 55.07307 moveto 0 0 rlineto stroke
194.6822 49.92838 moveto
(v) cmmi5 4.98132 fshow
198.2799 48.43398 moveto
(7) cmr5 4.98132 fshow
newpath 226.7716 58.1103 moveto 0 0 rlineto stroke
223.02866 52.9656 moveto
(v) cmmi5 4.98132 fshow
226.62636 51.4712 moveto
(8) cmr5 4.98132 fshow
newpath 255.11806 55.43294 moveto 0 0 rlineto stroke
251.3751 50.28824 moveto
(v) cmmi5 4.98132 fshow
254.97281 48.79384 moveto
(9) cmr5 4.98132 fshow
newpath 283.46451 57.82657 moveto 0 0 rlineto stroke
278.0265 52.68187 moveto
(v) cmmi5 4.98132 fshow
281.6242 51.18747 moveto
(10) cmr5 4.98132 fshow
showpage
%%EOF
