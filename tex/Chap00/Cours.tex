\chapter{Préparations}

\objectifintro{
Ce chapitre a pour objectif d'effectuer des révisions de bases des années précédentes : ensembles usuels, calculs fractionnaires et de puissances, développement et factorisation, équations et inéquations.
}

\begin{extrait}{Gottfried Wilhelm Leibniz (1646 -- 1717)}
J'ai trouvé cette chose étonnante : on peut représenter par les nombres toutes sortes de vérités.
\end{extrait}

\begin{objectifs}
\begin{numerote}
  \item \lienobj{1}{connaitre les ensembles usuels}
  \item savoir calculer et simplifier des résultats :
	\begin{itemize}
  \item \lienobj{2}{en simplifiant des fractions}
  \item \lienobj{3}{en développant et en factorisant}
  \item \lienobj{4}{en calculant avec des puissances}
  \item \lienobj{5}{en simplifiant des racines carrées}
	\end{itemize}
  \item \lienobj{6}{savoir résoudre des questions et inéquations}

\end{numerote}
\end{objectifs}

%%% Début du cours %%%
\section{Ensembles de nombres}

\begin{definition}[Ensembles usuels]
\labelobj{1}
On dispose des cinq ensembles usuels suivants :
\begin{itemize}
  \item L'ensemble $\N$ des \textbf{entiers naturels} \[ \N = \left \{ 0; 1; 2; 3; \hdots \right \}\]
  \item L'ensemble $\Z$ des \textbf{entiers relatifs} \[ \Z = \left \{\hdots; -4; -3; -2; -1; 0; 1; 2; 3; \hdots \right \}\]
  \item L'ensemble $\mathbb{D}$ des nombres décimaux, c'est-à-dire les nombres ayant un nombre fini de chiffres après la virgule.
  \item L'ensemble $\Q$ des nombres rationnels, c'est-à-dire les nombres s'écrivant sous la forme $\dfrac{p}{q}$, où $p$ est un entier relatif, et $q$ un entier naturel.
  \item L'ensemble $\R$ de l'ensemble des nombres réels.
\end{itemize}
\end{definition}

\begin{notation}
	On note $x\in \K$ pour indiquer que le nombre $x$ \textbf{appartient} à l'ensemble $\K$.
\end{notation}

\begin{exemple}
Par exemple, \[ \frac{1}{3}\in \Q, \quad 0,41 \in \mathbb{D}, \quad \sqrt{2}\in \R\]
\end{exemple}

\begin{definition}
	Soient $E$ et $F$ deux ensembles. On dit que $E$ est \textbf{inclus} dans $F$, et on note $E\subset F$, si tout élément $x$ de $E$ appartient également à $F$.
\end{definition}

\begin{propriete}
On dispose des inclusions suivantes :
\[ \N \subset \Z \subset \mathbb{D} \subset \Q \subset \R \]
\begin{center}
\includegraphics[scale=0.7]{ensembles.mps}
\end{center}
\end{propriete}

\ifprof
\begin{demonstration}
Par définition, $\N \subset \Z$, et $\Z \subset \mathbb{D}$ (car les entiers relatifs ont un nombre fini de chiffres après la virgule, à savoir $0$). $\Q\subset \R$ par définition également. Il reste à voir que $\mathbb{D} \subset \Q$.

Soit $a\in \mathbb{D}$. Alors $a$ possède un nombre fini de chiffres après la virgule; on note $d$ le nombre de chiffres après la virgule. Alors, \[ b=10^da \in \Z \] et donc \[ a = \frac{b}{10^d} \in \Q \]
\end{demonstration}
\else
\lignes{10}
\fi

\begin{notation}
On note \[ \R+ = [0;+\infty[ = \left \{ x\in \R,\quad x\geq 0\right \} \]
De même, on note
\[ \R- = ]-\infty;0] = \left \{ x\in \R,\quad x\leq 0 \right \} \]
On note $\R> = ]0;+\infty[$, et de même pour $\R<$. Les notations $+, -$ et $*$ s'étendent à tous les ensembles vus précédemment.
\end{notation}

\begin{exo}
Décrire les ensembles $\Z^*_-, \Z^+$, $\N^-$ et $\Q-$.
\end{exo}

\ifprof
\begin{soluce}
Rapidement, $\Z_-^*$ sont les entiers strictement négatifs, $\Z_+=\N$, $\N_-=\{0\}$ et $\Q-$ sont les rationnels négatifs.
\end{soluce}
\else
\lignes{5}
\fi

 \afaire{Exercice \lienexo{1}.}

\section{Calculs}

\subsection{Calcul fractionnaire}

Les règles de calcul sur les fractions doivent être maitrisées dès le début d'année. Rappelons-les :

\begin{propriete}
\labelobj{2} Pour tout $a\in \R$ et $b\in \R*$ :
\[ \frac{0}{b}=0\quad \quad \frac{a}{1}=a\quad\quad \frac{a}{-1}=-a\]
\[ \frac{a}{-b}=\frac{-a}{b}=-\frac{a}{b} \]
\[ \frac{a}{b}=a\frac{1}{b}=\frac{1}{b}a \]
\[ \frac{1}{\frac{a}{b}} = \frac{b}{a}; \]
pour $k\in \R*$ \[ \frac{a}{b}=\frac{k\times a}{k\times b} \]
et si $c\in \R$ et $d\in \R*$ :
\[ \frac{ \frac{a}{b}}{\frac{c}{d}} = \frac{a}{b}\times \frac{d}{c}\]
\end{propriete}

\begin{exo}
Calculer $\ds{A=\frac{\frac{3}{7}}{\frac{2}{5}}}$, $\ds{B=\frac{\frac{2}{3}}{6}}$, $C=3\times \dfrac{7}{18} \qeq D=\dfrac{4+17}{11+4}$.
\end{exo}

%\ifprof
\solution[8]{
En appliquant ce qui précède, on a rapidement : \[ A=\frac{15}{14},\quad B=\frac{1}{9},\quad C=\frac{7}{6} \]
Enfin, attention : on ne simplifie pas comme on le veut dans une fraction (il faut d'abord factoriser). Ici \[ D=\frac{21}{15}=\frac{7}{5} \]
}
%\else
%\lignes{8}
%\fi

 \afaire{Exercice \lienexo{2}.}

\subsection{Développement et factorisation}

\labelobj{3}
Développer une expression c'est l'écrire sous la forme d'une somme; factoriser c'est l'écrire sous la forme d'un produit. Par exemple :

\begin{description}
 \item 	$(x+1)(x+2) = x^2+3x+2$ est un développement	
 \item 	$x^2+2x = x(x+2)$ est une factorisation
\end{description}

Les identités suivantes sont à connaître :

\begin{propriete}[Identités remarquables]
On dispose des identités suivantes :
\begin{itemize}
  \item Pour tous réels $a$ et $b$ :
	\begin{align*}
		 (a+b)^2&=a^2+2ab+b^2\\
		 (a-b)^2&= a^2-2ab+b^2\\
		 a^2-b^2&=(a-b)(a+b)
	\end{align*}
  \item Pour tous réels $a$ et $b$ :
	\begin{align*}
		(a+b)^3 &= a^3+3a^2b+3ab^2+b^3\\
		(a-b)^3&= a^3-3a^2b+3ab^2-b^3\\
		a^3-b^3&= (a-b)(a^2+ab+b^2)\\
		a^3+b^3&= (a+b)(a^2-ab+b^2)
	\end{align*}
\end{itemize}
\end{propriete}

\begin{exo}
Développer, pour tout réel $x$, $(x+1)^2, (x+1)^3, (1-x)^2$ et $(1-x)^3$. Factoriser $4-x^2$ et $8+x^3$.
\end{exo}

\ifprof
\begin{soluce}
En utilisant les identités remarquables :
\begin{align*}
	(x+1)^2 &= x^2+2x+1\\
	(x+1)^3 &= x^3+3x^2+3x+1\\
	(1-x)^2 &= x^2-2x+1 \\
	(1-x)^3 &= 1-3x+3x^2-x^3\\
	4-x^2 &= (2-x)(2+x) \\
	8+x^3 &= (2+x)(4-2x+x^2)
\end{align*}
\end{soluce}
\else
\lignes{8}
\fi

 \afaire{Exercices \lienexo{6} et \lienexo{7}.}

\subsection{Puissances}
\labelobj{4}
Rappelons la définition de la puissance entière d'un nombre réel :

\begin{definition}
Soit $a$ un réel non nul, et $n\in \N$. On définit $a^n$ de la manière suivante :
\begin{itemize}
  \item $a^0=1$;
  \item $a^n = \underbrace{a\times a \times \cdots \times a}_{n\text{ fois}}$ si $n\geq 1$.
\end{itemize}
De plus, on note \[ a^{-n}=\frac{1}{a^n} \]
On définit ainsi $a^n$ pour $n\in \Z$.
\end{definition}

\begin{exemple}
On a $2^6 = 2\times 2 \times 2 \times 2 \times 2 \times 2 = 64$, $(-12)^1 = -12$ et $(1409091)^0 = 1$.
\end{exemple}

\begin{propriete}
Soient $a$ et $b$ deux réels non nuls, et $n, m$ deux entiers relatifs.
\begin{itemize}
  \item (puissances différentes) \[a^n\times a^m = a^{n+m};\quad \frac{a^n}{a^m} = a^{n-m}\qeq (a^n)^m = a^{nm};\]
  \item (puissances identiques) \[ a^n\times b^n=\left(a\times b\right)^n \qeq \frac{a^n}{b^n}=\left(\frac{a}{b}\right)^n.\]
\end{itemize}
\end{propriete}

\begin{exo}
Simplifier \[ A=\frac{5^4\times 3^{2}\times 2^{3}}{15^3} \qeq \frac{21\times 10^{-3}}{3\times 10^2}.\]
\end{exo}

\ifprof
\begin{soluce}
En utilisant la propriété précédente :
\begin{align*}
	A&= 5^1\times  3^{-1} \times 2^3 = \frac{40}{3} 	\\
	B&= \frac{21}{3}\times \frac{10^{-3}}{10^2} = 7\times 10^{-5}
\end{align*}
\end{soluce}
\else
\lignes{7}
\fi

 \afaire{Exercices \lienexo{3} et \lienexo{4}.}

\subsection{Racine carrée}
\labelobj{5}
Commençons par un rappel de la définition de la racine carrée d'un réel positif :

\begin{definition}[Racine carrée]
Soit $a \in \R+$. Il existe un \emph{unique} réel positif, noté $\sqrt{a}$, vérifiant \[ (\sqrt{a})^2 =a.\]
Ce nombre $\sqrt{a}$ est appelé \textbf{racine carrée} de $a$.
\end{definition}

\begin{remarque}
On dispose des valeurs remarquables suivantes : \[ \sqrt{0}=0,\quad \sqrt{1}=1,\quad \sqrt{4}=2\qeq \sqrt{9}=3 \]
\end{remarque}

\begin{attention}
On ne dispose pas de la racine carrée d'un nombre strictement négatif. Ainsi, la fonction racine carrée n'est définie que sur $\R_+$.
\end{attention}

\begin{propriete}
Soient $a$ et $b$ deux réels positifs. Alors
\[ \sqrt{a\times b} = \sqrt{a}\times \sqrt{b} \]
et si $b\neq 0$, alors \[ \sqrt{\frac{a}{b}} = \frac{\sqrt{a}}{\sqrt{b}}. \]
\end{propriete}

\begin{attention}
En règle général, $\ds{\sqrt{a+b} \neq \sqrt{a}+\sqrt{b}}$ ! Par exemple, $\sqrt{1+1}=\sqrt{2}$ et ce n'est pas égal à $\sqrt{1}+\sqrt{1}=2$.
\end{attention}

\begin{exo}
Simplifier $\sqrt{8}$, $\sqrt{48}$ et $\ds{\sqrt{\frac{9}{32}}}$.
\end{exo}

\ifprof
\begin{soluce}
Rapidement \[ \sqrt{8}=\sqrt{4 \times 2}=2\sqrt{2},\quad \sqrt{48}=4\sqrt{3} \qeq \sqrt{\frac{9}{32}}=\frac{3}{4\sqrt{2}}=\frac{3\sqrt{2}}{8}\]
\end{soluce}
\else
\lignes{7}
\fi

 \afaire{Exercice \lienexo{5}.}

\section{Equations, inéquations}
\labelobj{6}

\subsection{Equations}

Une équation est une égalité faisant apparaitre une, ou plusieurs inconnue(s). En général, les inconnues sont notées $x, y, z, \hdots$.

\begin{exemple}
L'équation $x^2+x=3-x$ est une équation faisant intervenir une variable. L'équation $x+y=\frac{1}{x}-\frac{1}{y}$ fait intervenir deux variables, $x$ et $y$.
\end{exemple}

\begin{definition}
\textbf{Résoudre} une équation, c'est déterminer l'ensemble de \emph{toutes} les valeurs de l'inconnues vérifiant l'équation; on parle alors d'une \textbf{solution} de l'équation, et on recherche toutes les solutions de l'équation.
\end{definition}

\begin{methode}
La première chose à faire est de trouver les valeurs interdites (division par $0$, par exemple).

Pour résoudre ensuite une équation, il n'y a pas de méthode universelle : on applique toutes propriétés possibles (multiplication, division) en raisonnant si possible par équivalence pour trouver les solutions.
\end{methode}

\begin{exo}
Résoudre l'équation \[ \frac{1}{x+2}+\frac{x+1}{x} = 1 \]
\end{exo}

\ifprof
\begin{soluce}
Déjà, il faut éviter $-2$ et $0$. On se place sur $\R \backslash \{-2; 0\}$. Alors, en multipliant par $x(x+2)$ (qui ne s'annule alors pas) :
\begin{align*}
	\frac{1}{x+2}+\frac{x+1}{x}=1  \text{ équivaut à } &x+(x+1)(x+2)=x(x+2)\\
	\text{ soit } &x+x^2+3x+2 = x^2+2x \\
	\text{ ce qui donne } &2x = -2 \\
	\text{ et donc } &x=-1
\end{align*}
Cette solution est autorisée (car différente de $0$ et $-2$). On a procédé par équivalence, et on peut conclure que l'ensemble des solutions est $\{ -1 \}$, ce qu'on note, en général \[ \mathcal{S}=\{-1\} \]
\end{soluce}
\else
\lignes{15}  
\fi

\begin{remarque}
On aurait pu, au lieu de mettre des mots en français, écrire $\Longleftrightarrow$. En général, on évitera l'enchaînement de symbole équivalent et implication, souvent illisible. On y reviendra dans le chapitre suivant.
\end{remarque}

 \afaire{Exercices \lienexo{8}, \lienexo{9} et \lienexo{10}.}

\subsection{Inéquations}

Une inéquation est une inégalité faisant apparaitre une, ou plusieurs inconnue(s).

Résoudre une inéquation, c'est trouver toutes les solutions de l'inéquation, c'est-à-dire trouver toutes les valeurs de l'inconnue vérifiant l'inégalité.

\begin{methode}
Pour résoudre une inéquation, on procède comme pour les équations, en utilisant les règles de calculs pour les inégalités.
\end{methode}

\begin{propriete}[Règles sur les inégalités]
Pour tous réels $x$ et $y$
\begin{itemize}
	\item Pour tout réel $k$, $x\leq y$ si et seulement si $x+k \leq y+k$;
	\item pour tout réel $k > 0$, $x\leq y$ si et seulement si $k\times x\leq k\times y$;
	\item pour tout réel $k < 0$, $x\leq y$ si et seulement si $k\times x\geq k\times y$;
\end{itemize}
On retiendra que multiplier par un nombre strictement positif conserve l'ordre, alors que multiplier par un nombre strictement négatif renverse l'ordre.
\end{propriete}

\begin{remarque}
La propriété précédente est valable en remplaçant $\leq$ par $<$, $>$ et $\geq$.
\end{remarque}

\begin{exemple}
Résoudre l'inéquation \[ -2(x-4)\geq 3x-2 \]
\end{exemple}

\ifprof
\begin{soluce}
Pour tout réel $x$ :
\begin{align*}
	-2(x-4)\geq 3x-2 \text{ soit } & -2x+8 \geq 3x-2 \\
	\text{ et donc } & -5x \geq -10 \\
	\text{ puis } &x\leq 2
\end{align*}
Ainsi, l'inégalité est vérifiée pour tout $x\leq 2$ : \[ \mathcal{S} = \left \{x\in \R, \quad x\leq 2\right \} = ]-\infty; 2] \]
\end{soluce}
\else
\lignes{15}
\fi

 \afaire{Exercices \lienexo{11}, \lienexo{12}, \lienexo{13}, \lienexo{14}, \lienexo{15}, \lienexo{16}, \lienexo{17} et \lienexo{18}.}
