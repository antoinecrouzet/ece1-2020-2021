///////////////////////////////////////////////////////////////////
//                       PLANCHE DE GALTON                       //
///////////////////////////////////////////////////////////////////
// Simulation d'une planche de Galton.                           //
// Lycée Pontus de Tyard, Chalon-sur-Saône.                      //
// Richard Pereyrol 2020.                                        //
// richard.pereyrol @ ac-dijon.fr                                //
///////////////////////////////////////////////////////////////////

// Utilisation :
// exécuter le programme puis taper 
// galton;
// pour simuler l'expérience avec 1000 billes.
// Taper entrée au début et à la fin si la densité de la loi
// normale doit être représentée.
// En pratique, placer la fenêtre graphique à côté de la console.

// Paramètres modifiables de la fonction galton, valeurs par défaut :
//N=1000;          //nombre de billes
//h=40;            //hauteur de la planche, nombre de niveaux
//limite_animation=1000;
//si N>limite_animation, seul le résultat est affiché,
//pas l'animation
//trace_grille=%T; //est-ce que la grille doit être tracée ?
//trace_norm=%T;   //tracé du graphe de la densité de la loi normale ?
//Par exemple :
//galton(N=10000,trace_grille=%F)
rand("seed",getdate("s"));

function galton(N,h,limite_animation,trace_grille,trace_norm)
    if ~exists('h','l') then h=40;
    end
    if ~exists('N','l') then N=1000;
    end
    if ~exists('limite_animation','l') then limite_animation=1000;
    end
    if ~exists('trace_grille','l') then trace_grille=%T;
    end
    if ~exists('trace_norm','l') then trace_norm=%T;
    end
    D=grand(h,N,"uin",0,1)-1/2; //les changements d'abscisse 
    // des billes : +1/2 ou -1/2 à chaque niveau
    A=sum(D,"r"); //les abscisses finales des billes
    B=tabul(A,"i"); //le nombre de billes par abscisse finale
    S=cumsum(D,"r"); //toutes les positions intermédiaires de billes
    a=min(A);b=max(A); //abscisses finales minimale et maximale
    c=floor(max(abs(min(S)),abs(max(S)))+1);
    //Réglages des graphiques.
    clf();
    subplot(2,1,2);
    axe2=gca();
    axe2.data_bounds=[-c,0;c,max(max(B(:,2)/N),1/sqrt(h*%pi/2))];
    axe2.tight_limits="on";
    plot2d(0,0);
    subplot(2,1,1);
    axe1=gca();
    axe1.x_ticks=tlist(["ticks", "locations", "labels"],[-c:c],string([-c:c]));
    axe1.tight_limits="on";
    axe1.data_bounds=[-c,0;c,h+1/2];
    //Tracé de la grille.
    if trace_grille then
        G=zeros(2,h*(h+1)/2);  
        // G contient les positions des "clous" de la grille
        ind=1;
        for j=0:h-1
            ind=ind+j;
            G(1,ind:(ind+j))=[0:j]-j/2;
            G(2,ind:(ind+j))=h-j;
        end
        G=G(:,find(abs(G(1,:))<=c)); // tronque la grille
        plot2d(G(1,:),G(2,:),-6);    // trace la grille
        e=gce();
        e.children.mark_foreground=2;
        e.children.mark_size_unit ="point";
        e.children.mark_size=6;
    end
    input("Taper Entrée pour démarrer.");
    //Affichage du résultat, avec ou sans "animation"
    if N<=limite_animation then 
        plot2d(0,h,-6);
        ligne=gce();
        C=zeros(1,b-a+1);
        E=[A;A-a+1];
        for i=1:N
            subplot(2,1,1);
            delete(ligne);
            //Trace la trajectoire de la bille.
            plot2d(cumsum([0;D(:,i)])',h-[0:h]);
            ligne=gce();
            ligne.children.thickness=2;
            title('Bille '+string(i)+"/"+string(N));
            subplot(2,1,2);
            j=E(2,i);
            C(j)=C(j)+1/N;
            //Augmente la hauteur de la colonne d'arrivée
            //        if scilab5 then xset("color",5);
            xfrect(-.4+A(i),C(j),.8,C(j));
            barre=gce();
            barre.background = color("red");
            xrect(-.4+A(i),C(j),.8,C(j));
            contourBarre=gce();
            contourBarre.background = color("red");
        end
    else
        subplot(2,1,2);
        plot2d(0,0);
        bar(B(:,1),B(:,2)/N,"red");
    end
    axe2=gca();
    axe2.x_ticks=axe1.x_ticks;
    //Densité de la loi normale associée pour comparaison.
    function y=phi(x)
        y=exp(-x.^2/2/(sqrt(h)/2)^2)/sqrt(2*%pi)/(sqrt(h)/2);
    endfunction
    if N<=limite_animation then delete(ligne);
    end
    if trace_norm then 
        input("Taper Entrée pour finir.");
        fplot2d(linspace(-c,c,100),phi,style=color("black"));
    end
endfunction
